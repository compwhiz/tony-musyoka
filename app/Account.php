<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    public function reviews()
    {
        return $this->hasMany(Review::class);
   }
}
