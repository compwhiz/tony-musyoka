<?php

namespace App\Http\Controllers;

use App\Email;
use Illuminate\Http\Request;

class EmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Email $email
     * @return \Illuminate\Http\Response
     */
    public function show(Email $email)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Email $email
     * @return \Illuminate\Http\Response
     */
    public function edit(Email $email)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Email $email
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Email $email)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Email $email
     * @return \Illuminate\Http\Response
     */
    public function destroy(Email $email)
    {
        //
    }


    public function cpanel()
    {
        // Get cpanel email accounts
        $emails = new \App\WHM\WHM();
        $resp = \Illuminate\Support\Facades\Cache::remember('mai', '9000', function () use ($emails) {
            return $emails->getresponse("cpanel?cpanel_jsonapi_user=compwhizco&cpanel_jsonapi_apiversion=2&cpanel_jsonapi_module=Email&cpanel_jsonapi_func=listpopssingle");
        });
        $e = collect($resp['cpanelresult']['data'])->map(function ($e) {
            return $e['email'];
        })->each(function ($mail) use ($emails) {
            $email = explode('@', $mail);
            $domain = trim($email[1]);
            $emailUsername = trim($email[0]);
            $changePassword = $emails->getresponse("cpanel?cpanel_jsonapi_user=compwhizco&cpanel_jsonapi_apiversion=2&cpanel_jsonapi_module=Email&cpanel_jsonapi_func=passwdpop&domain={$domain}&email={$emailUsername}&password=E9A.j-@*fBhRU'rb");
            $client = new \Webklex\IMAP\Client([
                'host'          => $email[1],
                'port'          => 993,
                'encryption'    => 'ssl',
                'validate_cert' => true,
                'username'      => $mail,
                'password'      => "E9A.j-@*fBhRU'rb",
                'protocol'      => 'imap'
            ]);
            $client->connect();
            $aFolder = $client->getFolder('INBOX');
            $messag = $aFolder->messages()->all()->get();
            $formattedMail = collect($messag)->mapWithKeys(function ($message) {
                $mail = collect($message->getFrom()[0])['mail'];
                return [$mail => [
                    $message->getSubject(),
                    $message->getTextBody(),
                    $message->getTextBody(),
                ]
                ];

            });

//            $saveMail = new Email();
//            $saveMail->message =
            dd($formattedMail);
        });

    }


    public function getEmails()
    {
        // Get an active account

        // Reset the password

        // Fetch all the emails


        /// Web App
        /// Train young people.
        ///
        /// Connect them to the markert.
        /// Commission based

    }
}
