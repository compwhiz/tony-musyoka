<?php

namespace App\Nova;

use App\Nova\Actions\GetEmail;
use App\Nova\Metrics\TotalAccounts;
use App\Nova\Metrics\TotalEarnings;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class Account extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Account';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name',
    ];
    public static $group = 'G2 Reviews';

    public static $with = [
        'reviews',
    ];
    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
//            ID::make()->sortable()->onlyOnIndex(),
            Text::make('Email','account_email'),
            Text::make('Name','name'),
            Text::make('Password','password')->onlyOnForms(),
            HasMany::make('reviews'),
            Text::make('Reviews', function () {
                return $this->reviews->count();
            }),
            Text::make('Income', function () {
                return '$ '.$this->reviews->where('reimbursed',true)->count() * 10;
            }),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [new TotalAccounts(),new TotalEarnings()];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [new GetEmail()];
    }
}
