<?php

namespace App\Nova\Lenses;

use App\Nova\Account;
use App\Nova\Actions\MarkAsPaid;
use App\Nova\Actions\markAsRejected;
use App\Nova\Filters\PaidReviews;
use App\Nova\Filters\RejectedReviews;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Lenses\Lens;
use Laravel\Nova\Http\Requests\LensRequest;

class UnpaidReviews extends Lens
{
    /**
     * Get the query builder / paginator for the lens.
     *
     * @param  \Laravel\Nova\Http\Requests\LensRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return mixed
     */
    public static function query(LensRequest $request, $query)
    {
        return $request->withOrdering($request->withFilters(
            $query->where('rejected',false)->where('reimbursed',false)

        ));
    }

    /**
     * Get the fields available to the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make('ID', 'id')->sortable()->onlyOnIndex(),
            Text::make('Review','review_name'),
            Date::make('Date Reviewed','date_review_done'),
            BelongsTo::make('Reviewer','account', Account::class),

        ];
    }

    /**
     * Get the filters available for the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new PaidReviews(),
            new RejectedReviews()
        ];
    }

    /**
     * Get the actions available on the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [new MarkAsPaid(),new markAsRejected()];
    }

    /**
     * Get the URI key for the lens.
     *
     * @return string
     */
    public function uriKey()
    {
        return 'unpaid-reviews';
    }
}
