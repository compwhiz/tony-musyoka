<?php

namespace App\Nova;

use App\Nova\Actions\MarkAsPaid;
use App\Nova\Actions\markAsRejected;
use App\Nova\Filters\PaidReviews;
use App\Nova\Filters\RejectedReviews;
use App\Nova\Lenses\UnpaidReviews;
use App\Nova\Metrics\TotalEarnings;
use App\Nova\Metrics\TotalReviews;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class Review extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Review';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'review_name';
    public static $group = 'G2 Reviews';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'review_name',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
//            Text::make('account_id'),
            Text::make('Product Reviewed','review_name'),
            Date::make('Date review done','date_review_done'),
            BelongsTo::make('account')->searchable(),
            Boolean::make('Paid','reimbursed'),
            HasOne::make('payment'),
            Boolean::make('Rejected', 'rejected'),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [new TotalEarnings(),new TotalReviews()];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new PaidReviews(),
            new RejectedReviews()
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [new UnpaidReviews(),new \App\Nova\Lenses\PaidReviews()];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [new MarkAsPaid(),new markAsRejected()];
    }
}
