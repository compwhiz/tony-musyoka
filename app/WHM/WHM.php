<?php


namespace App\WHM;


class WHM
{
    protected $response;
    protected $client;
    public function __construct()
    {

        $this->client = new \GuzzleHttp\Client();

    }


    public function getresponse($url=null)
    {
        $user = config('whm.username');
        $token = config('whm.token');

        $this->response = $this->client->request('GET', "https://gains.supermonsterservers.com:2087/json-api/{$url}", [
            'headers' => [
                'Authorization' => "Basic ". base64_encode("$user".":"."$token"),
            ],
        ]);
        return $this->getBody();

    }

    protected function getBody()
    {
        return json_decode((string) $this->response->getBody(), true);
    }
}
