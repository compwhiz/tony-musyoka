<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href="css/style.css" rel="stylesheet">
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/slider.css">
    <link rel="stylesheet" type="text/css" href="lib/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="lib/slick/slick-theme.css">
    <link href="css/portfolio.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/icofont.min.css">
    <link rel="stylesheet" type="text/css" href="css/icofont.css">
</head>

<body oncontextmenu="return false;">

<div class="header">
    <nav>
        <div class="container">
            <div class="row">
                <nav class="navbar navbar-expand-md navbar-light fixed-top sticky-top nav-menu">
                    <div class="col-md-4 col-xs-12 col-sm-6 padding">
                        <div class="logo">
                            <a href="#">
                                <img src="images/logo.png" alt="" title="" >
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 padding">
                        <div class="menu">


                            <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarNav">
                                <span class="navbar-toggler-icon "></span>
                            </button>
                            <div class="collapse navbar-collapse menu" id="navbarNav">
                                <ul class="navbar-nav ml-auto">
                                    <li>
                                        <a class="active" href="index.html">Home</a>
                                    </li>
                                    <li>
                                        <a href="about.html">About us</a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Portfolio</a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="portfolio.html">portfolio</a></li>
                                            <li><a href="portfolio2.html">portfolio 2</a></li>
                                            <li><a href="portfolio3.html">portfolio 3</a></li>

                                        </ul>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Our Blog</a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="blog.html">Our BLog</a></li>
                                            <li><a href="blog1.html">Our Blog 2</a></li>
                                            <li><a href="singleblog.html">Single Blog</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="contact.html">Contact</a>
                                    </li>
                                </ul>

                            </div>

                        </div>

                    </div>
                    <div class="col-md-2 padding col-xs-12">

                        <div class="social">
                            <ul>
                                <li><a href="#"><i class="icofont-facebook"></i></a></li>
                                <li><a href="#"><i class="icofont-twitter"></i></a></li>
                                <li><a href="#"><i class="icofont-google-plus"></i></a></li>
                                <li><a href="#"><i class="icofont-instagram"></i></a></li>

                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </nav>
</div>

<!----#Slider--->
<!--#Slider Start-->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>
    <!-- Wrapper for slides -->
    <div class="carousel-inner">

        <div class="item active">
            <img src="images/slider/Banner.jpg" alt="Banner">
            <div class="carousel-caption animated">
                <h3>Hello, I am a <span>Professional </span><br />
                    <p>Photographer</p>
                </h3>
                <div class="slider-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras pharetra dui eget tempus consectetur.
                    Vivamus tincidunt sed neque vitae aliquam. Nam varius tristique justo, vel congue nunc aliquet nec.
                </div>
                <br /><button type="submit" class="our-port">Read More</button> &nbsp; &nbsp;
            </div>
        </div><!-- End Item -->
        <div class="item">
            <img src="images/slider/Banner.jpg" alt="Banner">
            <div class="carousel-caption animated">
                <h3>Hello,  I am a <span>Professional </span><br />
                    <p>Photographer</p>
                </h3>
                <div class="slider-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras pharetra dui eget tempus consectetur.
                    Vivamus tincidunt sed neque vitae aliquam. Nam varius tristique justo, vel congue nunc aliquet nec.
                </div>
                <br /><button type="submit" class="our-port">Read More</button> &nbsp; &nbsp;
            </div>
        </div><!-- End Item -->

        <div class="item">
            <img src="images/slider/Banner.jpg" alt="Banner">
            <div class="carousel-caption animated">
                <h3>Hello, I am a <span>Professional </span><br />
                    <p>Photographer</p>
                </h3>
                <div class="slider-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras pharetra dui eget tempus consectetur.
                    Vivamus tincidunt sed neque vitae aliquam. Nam varius tristique justo, vel congue nunc aliquet nec.
                </div>
                <br /><button type="submit" class="our-port">Read More</button> &nbsp; &nbsp;
            </div>
        </div><!-- End Item -->

        <div class="item">
            <img src="images/slider/Banner.jpg" alt="Banner">
            <div class="carousel-caption animated">
                <h3>Hello, I am a <span>Professional </span><br />
                    <p>Photographer</p>
                </h3>
                <div class="slider-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras pharetra dui eget tempus consectetur.
                    Vivamus tincidunt sed neque vitae aliquam. Nam varius tristique justo, vel congue nunc aliquet nec.
                </div>
                <br /><button type="submit" class="our-port">Read More</button> &nbsp; &nbsp;
            </div>
        </div><!-- End Item -->

    </div><!-- End Carousel Inner -->

</div><!-- End Carousel -->

<!--#Slider End-->
<!--#Title Main Page-->
<div class="main-title">
    <div class="container">
        <div clas="row">
            <div class="col-md-2 padding col-xs-12">
                <div class="page-title">
                    MY STUDIO DETAIL<br />
                    <span>ABOUT US</span>
                </div>
            </div>
            <div class="col-md-2 hidden-xs  padding col-xs-4">
                <img src="images/Line.gif" alt="" title="">
            </div>
            <div class="col-md-1 hidden-xs  padding col-xs-4">
                <img src="images/Log.png" alt="" title="">
            </div>
            <div class="col-md-7 titile-cont  col-xs-12">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nibh erat, semper at pretium in, efficitur nec leo. Nullam ut leo augue.
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="container">
        <div class="row">
            <div class="about-text">
                <div class="col-md-7 titile-cont  col-xs-12">

                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eleifend fringilla massa blandit mattis. Nunc sed volutpat dui. Integer maximus mollis magna in
                        cursus. Cras finibus, nunc in dignissim semper, enim diam pellentesque sem, vel consectetur turpis turpis vitae urna. Aliquam erat volutpat.
                        Aenean quis turpis consectetur felis porttitor euismod sit amet id dui. Vestibulum cursus sodales risus in accumsan. Nullam ullamcorper eros id
                        ligula euismod, sit amet tincidunt nibh dignissim. Cras vel lorem rutrum ligula rutrum semper. Sed semper, lorem nec molestie convallis,
                        leo eros dapibus magna, ac varius est sapien blandit libero. Cras scelerisque neque id mi commodo, in lobortis elit placerat. Aenean a
                        arcu malesuada, malesuada purus dictum, ullamcorper enim. Pellentesque eleifend blandit neque, vitae aliquet arcu ornare ut. Proin luctus,
                        nibh a pulvinar tincidunt, 	ante purus sollicitudin augue, vel tincidunt velit elit sit amet erat. Etiam in sagittis nulla. </p>
                    <br />
                    <div class="view text-left">
                        <a href="#">READ MORE</a>
                    </div>
                </div>
                <div class="col-md-5 padding col-xs-12">
                    <img src="images/photo.png" alt="" title="" >
                </div>
            </div>
        </div>
    </div>
</div>
<!--#Title Main About Page End-->
</div>
<div class="container">
    <div class="row">

        <div class="clearfix"></div>
    </div>
</div>
<div class="box">
    <div class="container">
        <div class="row">

            <div class="col-md-3 col-xs-12 col-sm-6 padding">
                <div class="boxes">
                    <div class="box-ico">
                        <i class="icofont-light-bulb"></i>

                    </div>
                    <div class="box-title">
                        We are Creative
                    </div>
                    <div class="box-content">
                        Curabitur dapibus nulla velit, tincidunt lobortis lorem molestie in. Aliquam malesuada, risus sit amet vulputate placerat, felis elit dictum dolor.
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-12 col-sm-6 padding">
                <div class="boxes">
                    <div class="box-ico">
                        <i class="icofont-stopwatch"></i>
                    </div>
                    <div class="box-title">
                        Time Every Where
                    </div>
                    <div class="box-content">
                        Curabitur dapibus nulla velit, tincidunt lobortis lorem molestie in. Aliquam malesuada, risus sit amet vulputate placerat, felis elit dictum dolor.
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-12 col-sm-6 padding">
                <div class="boxes">
                    <div class="box-ico">
                        <i class="icofont-video-cam"></i>
                    </div>
                    <div class="box-title">
                        Video Motion
                    </div>
                    <div class="box-content">
                        Curabitur dapibus nulla velit, tincidunt lobortis lorem molestie in. Aliquam malesuada, risus sit amet vulputate placerat, felis elit dictum dolor.
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-12 col-sm-6 padding">
                <div class="boxes">
                    <div class="box-ico">
                        <i class="icofont-song-notes"></i>
                    </div>
                    <div class="box-title">
                        Hardened Metals
                    </div>
                    <div class="box-content">
                        Curabitur dapibus nulla velit, tincidunt lobortis lorem molestie in. Aliquam malesuada, risus sit amet vulputate placerat, felis elit dictum dolor.
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!---Box End-->
<!---Portfoli Start-->
<div class="main-title">
    <div class="container">
        <div clas="row">
            <div class="col-md-2 padding col-xs-12">
                <div class="page-title">
                    Exclusive work on<br />
                    <span>My Portfolio</span>
                </div>
            </div>
            <div class="col-md-2 hidden-xs padding col-xs-4">
                <img src="images/Line.gif" alt="" title="">
            </div>
            <div class="col-md-1 hidden-xs padding col-xs-4">
                <img src="images/Log.png" alt="" title="">
            </div>
            <div class="col-md-7 titile-cont  col-xs-12">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nibh erat, semper at pretium in, efficitur nec leo. Nullam ut leo augue.
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="container">
        <div class="row">
            <div class="portfolio">
                <div class="toolbar mb2 mt2">
                    <button class="btn fil-cat" href="#" data-rel="all">All</button>
                    <button class="btn fil-cat" data-rel="Design">Design</button>
                    <button class="btn fil-cat" data-rel="Wedding">Wedding</button>
                    <button class="btn fil-cat" data-rel="Sports">Sports</button>
                    <button class="btn fil-cat" data-rel="Fashion">Fashion</button>
                    <button class="btn fil-cat" data-rel="Travels">Travels</button>
                    <button class="btn fil-cat" data-rel="Party">Party</button>
                    <button class="btn fil-cat" data-rel="Animal">Animal</button>
                    <button class="btn fil-cat" data-rel="LandScap">LandScap</button>
                </div>

                <div class="clearfix"></div>
                <div id="portfolio">
                    <div class="tile  scale-anm Design all">
                        <img src="images/portfolio/gallery_01.jpg" alt="" />
                    </div>
                    <div class="tile scale-anm Sports all">
                        <img src="images/portfolio/gallery_02.jpg" alt="" />
                    </div>
                    <div class="tile scale-anm Travels all">
                        <img src="images/portfolio/gallery_03.jpg" alt="" />
                    </div>
                    <div class="tile scale-anm Design all">
                        <img src="images/portfolio/gallery_04.jpg" alt="" />
                    </div>
                    <div class="tile scale-anm Wedding all">
                        <img src="images/portfolio/gallery_05.jpg" alt="" />
                    </div>
                    <div class="tile scale-anm Sports all">
                        <img src="images/portfolio/gallery_06.jpg" alt="" />
                    </div>
                    <div class="tile scale-anm Travels all">
                        <img src="images/portfolio/gallery_07.jpg" alt="" />
                    </div>
                    <div class="tile scale-anm Party all">
                        <img src="images/portfolio/gallery_08.jpg" alt="" />
                    </div>
                    <div class="tile scale-anm Wedding all">
                        <img src="images/portfolio/gallery_09.jpg" alt="" />
                    </div>
                    <div class="tile scale-anm Animal all">
                        <img src="images/portfolio/gallery_01.jpg" alt="" />
                    </div>
                    <div class="tile scale-anm Party all">
                        <img src="images/portfolio/gallery_02.jpg" alt="" />
                    </div>
                    <div class="tile scale-anm Design all">
                        <img src="images/portfolio/gallery_03.jpg" alt="" />
                    </div>
                    <div class="tile scale-anm LandScap all">
                        <img src="images/portfolio/gallery_01.jpg" alt="" />
                    </div>

                    <div class="tile scale-anm Sports all">
                        <img src="images/portfolio/gallery_03.html" alt="" />
                    </div>

                    <div class="tile scale-anm Fashion all">
                        <img src="images/portfolio/gallery_05.jpg" alt="" />
                    </div>


                    <div class="tile scale-anm Fashion all">
                        <img src="images/portfolio/gallery_08.jpg" alt="" />
                    </div>
                    <div class="tile scale-anm LandScap all">
                        <img src="images/portfolio/10.html" alt="" />
                    </div>
                </div>

                <div class="view text-center">
                    <a href="portfolio.html">VIEW MORE</a>
                </div>
            </div>


            <div class="clearfix"></div>


        </div>

    </div>

</div>
<!---Portfoli end-->
<div class="social-text">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-xs-12 col-sm-6 padding">
                <div class="icon-main">
                    <img src="images/icon/icon1.png" alt="" title="">
                </div>
                <div class="ico-no">
                    965
                </div>
                <div class="ico-txt">
                    Happy Customers
                </div>
            </div>
            <div class="col-md-3 col-xs-12 col-sm-6 padding">
                <div class="icon-main">
                    <img src="images/icon/icon2.png" alt="" title="">
                </div>
                <div class="ico-no">
                    5k+
                </div>
                <div class="ico-txt">
                    Photo Sessions
                </div>
            </div>
            <div class="col-md-3 col-xs-12 col-sm-6 padding">
                <div class="icon-main">
                    <img src="images/icon/icon3.png" alt="" title="">
                </div>
                <div class="ico-no">
                    2k+
                </div>
                <div class="ico-txt">
                    video Sessions
                </div>
            </div>
            <div class="col-md-3 col-xs-12 col-sm-6 padding">
                <div class="icon-main">
                    <img src="images/icon/icon4.png" alt="" title="">
                </div>
                <div class="ico-no">
                    30k+
                </div>
                <div class="ico-txt">
                    Archive Photographs
                </div>
            </div>

        </div>
    </div>
</div>
<!---#Icon Box End--->
<!---#Team Member--->
<div class="main-title">
    <div class="container">
        <div clas="row">
            <div class="col-md-2 padding col-xs-4">
                <div class="page-title">
                    What people say<br />
                    <span>Testimonials</span>
                </div>
            </div>
            <div class="col-md-2 hidden-xs  padding col-xs-4">
                <img src="images/Line.gif" alt="" title="">
            </div>
            <div class="col-md-1 hidden-xs  padding col-xs-4">
                <img src="images/Log.png" alt="" title="">
            </div>

            <div class="col-md-7 titile-cont  col-xs-12">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nibh erat, semper at pretium in, efficitur nec leo. Nullam ut leo augue.
            </div>
        </div>
        <div class="clearfix"></div>

    </div>
    <div class="container">
        <div class="testimonialbg">
            <div class="comts">
                <i class="icofont-quote-right"></i>
            </div>

            <section role="complementary" class="simple quotes no-fouc container">
                <blockquote class="quote-card">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ante diam, malesuada quis lacinia vitae,
                    volutpat at lacus. Integer vestibulum pellentesque ante sed ultrices. Phasellus nec pulvinar augue.
                    Donec ut accumsan est. Nulla tempor pulvinar dapibus.
                    <div class="name">John Doe</div>
                </blockquote>
                <blockquote class="quote-card">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ante diam, malesuada quis lacinia vitae,
                    volutpat at lacus. Integer vestibulum pellentesque ante sed ultrices. Phasellus nec pulvinar augue. Donec ut accumsan est.
                    Nulla tempor pulvinar dapibus.
                    <div class="name">John Doe</div>
                </blockquote>
                <blockquote class="quote-card">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ante diam, malesuada quis lacinia vitae,
                    volutpat at lacus. Integer vestibulum pellentesque ante sed ultrices. Phasellus nec pulvinar augue.
                    Donec ut accumsan est. Nulla tempor pulvinar dapibus.
                    <div class="name">John Doe</div>
                </blockquote>
            </section>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>
    <!--<div class="container">

            <div class="testiminal-image">
                <ul>
                <li><a href="#"><img src="images/testimonial/testi1.jpg" title="" alt="testimonial"></a></li>
                <li><a href="#"><img src="images/testimonial/testi1.jpg" title="" alt="testimonial"></a></li>
                <li><a href="#"><img src="images/testimonial/testi.jpg" title="" alt="testimonial"></a></li>
                <li><a href="#"><img src="images/testimonial/testi1.jpg" title="" alt="testimonial"></a></li>
                <li><a href="#"><img src="images/testimonial/testi.jpg" title="" alt="testimonial"></a></li>
                <li><a href="#"><img src="images/testimonial/testi1.jpg" title="" alt="testimonial"></a></li>
                <li><a href="#"><img src="images/testimonial/testi.jpg" title="" alt="testimonial"></a></li>
                <li><a href="#"><img src="images/testimonial/testi1.jpg" title="" alt="testimonial"></a></li>
                <li><a href="#"><img src="images/testimonial/testi.jpg" title="" alt="testimonial"></a></li>
                </ul>
            </div>

    </div>--->
</div>
<!--#testimonial End-->
<!--#Blog Start-->
<div class="blog-home">
    <div class="container">
        <div clas="row">
            <div class="col-md-2 padding col-xs-12">
                <div class="page-title">
                    Latest News For Blog<br />
                    <span>Last News</span>
                </div>
            </div>
            <div class="col-md-2 hidden-xs padding col-xs-4">
                <img src="images/Line.gif" alt="" title="">
            </div>
            <div class="col-md-1 hidden-xs padding col-xs-4">
                <img src="images/Log.png" alt="" title="">
            </div>

            <div class="col-md-7 titile-cont  col-xs-12">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nibh erat, semper at pretium in, efficitur nec leo. Nullam ut leo augue.
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="container midd-blog">

        <div class="col-md-6 col-xs-12 col-xs-12 p-left">
            <div class="blogimg">
                <img src="images/blog/blog/blog_01.jpg" alt="blog" title="">
            </div>
            <div class="blogbg">
                <div class="blog-title">
                    <a href="blog.html">Blog Title here</a>
                </div>
                <div class="home-admin">
                    <ul>
                        <li><a href="blog.html">by Admin</a></li>
                        <li>|</li>
                        <li><a href="blog.html">22 August</a></li>
                    </ul>
                </div>
                <div class="homgbgcontent">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Nam ante diam, malesuada quis lacinia vitae, volutpat at lacus.
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xs-12 col-xs-12 p-right">
            <div class="blogimg">
                <img src="images/blog/blog/blog_02.jpg" alt="blog" title="">
            </div>
            <div class="blogbg">
                <div class="blog-title">
                    <a href="blog.html">Blog Title here</a>
                </div>
                <div class="home-admin">
                    <ul>
                        <li><a href="blog.html">by Admin</a></li>
                        <li>|</li>
                        <li><a href="blog.html">22 August</a></li>
                    </ul>
                </div>
                <div class="homgbgcontent">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Nam ante diam, malesuada quis lacinia vitae, volutpat at lacus.
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xs-12 col-xs-12 p-left">
            <div class="blogimg">
                <img src="images/blog/blog/blog_03.jpg" alt="blog" title="">
            </div>
            <div class="blogbg">
                <div class="blog-title">
                    <a href="blog.html">Blog Title here</a>
                </div>
                <div class="home-admin">
                    <ul>
                        <li><a href="blog.html">by Admin</a></li>
                        <li>|</li>
                        <li><a href="blog.html">22 August</a></li>
                    </ul>
                </div>
                <div class="homgbgcontent">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Nam ante diam, malesuada quis lacinia vitae, volutpat at lacus.
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xs-12 col-xs-12 p-right">
            <div class="blogimg">
                <img src="images/blog/blog/blog_04.jpg" alt="blog" title="">
            </div>
            <div class="blogbg">
                <div class="blog-title">
                    <a href="blog.html">Blog Title here</a>
                </div>
                <div class="home-admin">
                    <ul>
                        <li><a href="blog.html">by Admin</a></li>
                        <li>|</li>
                        <li><a href="blog.html">22 August</a></li>
                    </ul>
                </div>
                <div class="homgbgcontent">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Nam ante diam, malesuada quis lacinia vitae, volutpat at lacus.
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<!--#Blog End-->
<!---#contact Form--->
<div class="main-title">
    <div class="container">
        <div clas="row">
            <div class="col-md-2 padding col-xs-12">
                <div class="page-title">
                    Get in Touch<br />
                    <span>Contact US</span>
                </div>
            </div>
            <div class="col-md-2 hidden-xs  padding col-xs-4">
                <img src="images/Line.gif" alt="" title="">
            </div>
            <div class="col-md-1 hidden-xs padding col-xs-4">
                <img src="images/Log.png" alt="" title="">
            </div>

            <div class="col-md-7 titile-cont  col-xs-12">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nibh erat, semper at pretium in, efficitur nec leo. Nullam ut leo augue.
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="container home-form">
        <div class="row">

            <div class="col-md-4" data-for="name">
                <div class="form">
                    <label class="form-control-label mbr-fonts-style display-7" for="name-form1-2w">Name *</label>
                    <input type="text" class="form-control" name="name" data-form-field="Name" required="" placeholder="Type your name" id="name-form1-2w">
                </div>
            </div>
            <div class="col-md-4 multi-horizontal" data-for="email">
                <div class="form-group">
                    <label class="form-control-label mbr-fonts-style display-7" for="email-form1-2w">Email *</label>
                    <input type="email" class="form-control" name="email" data-form-field="Email" placeholder="Type your Email" required="" id="email-form1-2w">
                </div>
            </div>
            <div class="col-md-4 multi-horizontal" data-for="phone">
                <div class="form-group">
                    <label class="form-control-label mbr-fonts-style display-7" for="phone-form1-2w">Phone Number *</label>
                    <input type="tel" class="form-control" name="phone" data-form-field="Phone" placeholder="Type your Phone Number" id="phone-form1-2w">
                </div>
            </div>
            <div class="col-sm-12" data-for="message">
                <label class="form-control-label mbr-fonts-style display-7" for="message-form1-2w">Message *</label>
                <textarea type="text" class="form-control" name="message" rows="7" data-form-field="Message" placeholder="Type your Message" id="message-form1-2w"></textarea>
                <span class="input-group-btn">
                            <button href="#" type="submit" class="btn btn-primary btn-form display-4">SEND Message</button>
                        </span>
            </div>

        </div>
    </div>
</div>
<!--Footer-->
<footer>
    <div class="footer">
        <div class="container">
            <div class="icon">
                <div class="row">

                    <div class="col-md-4 footer-box padding col-xs-12">
                        <i class="icofont-phone"></i><br>
                        <p> 1111-222-4444
                            <br>
                            1 800 12345 Hello</p>

                    </div>
                    <div class="col-md-4 footer-box padding col-xs-12">
                        <i class="icofont-envelope"></i>
                        <p>dsstudio@example.com<br>
                            info@dsstdio.com</p>
                    </div>
                    <div class="col-md-4 footer-box padding col-xs-12">

                        <i class="icofont-google-map"></i>

                        <p>66 Town St- Suite 522<br>
                            Newyork,United States,GA11111</p>
                    </div>

                </div>

            </div>

        </div>
        <div class="container">
            <div class="text-center">
                <img src="images/logo.png" alt="" title="" >
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="social-icon">
            <ul>
                <li><a href="#"><i class="icofont-facebook"></i></a></li>
                <li><a href="#"><i class="icofont-twitter"></i></a></li>
                <li><a href="#"><i class="icofont-google-plus"></i></a></li>
                <li><a href="#"><i class="icofont-instagram"></i></a></li>
                <li><a href="#"><i class="icofont-linkedin"></i></a></li>

                <li><a href="#"><i class="icofont-youtube-play"></i></a></li>

            </ul>
        </div>
    </div>
    <a id="back2Top" title="Back to top" href="#" style="display: inline;">➤</a>
    <div class="footer-bottom">
        Copyright @ 2018 &nbsp; | <a href="#">DS Studio</a>| &nbsp;  All Right Reserved.

    </div>
</footer>

<script type="text/javascript" src="lib/slick/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/dropdown.js"></script>
<script type="text/javascript" src="js/disabled.js"></script>
<script src="js/portfolio.js"></script>
<script src="js/slider.js"></script>
<script type="text/javascript" src="lib/slick/slick.min.js"></script>
<script>

    $('.quotes').slick({
        dots: true,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 6000,
        speed: 800,
        slidesToShow: 1,
        adaptiveHeight: true
    });


    $( document ).ready(function() {
        $('.no-fouc').removeClass('no-fouc');
    });
</script>
</body>
</html>
