<p class="mt-8 text-center text-xs text-80">
    <a href="https://compwhiz.co.ke" class="text-primary dim no-underline">G2 Income Tracker</a>
    <span class="px-1">&middot;</span>
    &copy; {{ date('Y') }} Compwhiz LLC - By Kennedy Mutisya.
    <span class="px-1">&middot;</span>
    v2.1.2
</p>
