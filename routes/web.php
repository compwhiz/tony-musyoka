<?php

Route::get('/', function () {
    return view('welcome');
});

Route::view('about-tony-musyoka-photography', 'aboutus');

Route::view('our-team', 'team');

Route::view('faqs', 'faqs');

Route::view('pricing', 'pricing');

Route::view('pricing', 'pricing');

Route::view('cases', 'cases');

Route::view('services', 'services');

Route::view('blog', 'blog');

Route::view('contact', 'contact');

Route::get('clear', function () {
    \Illuminate\Support\Facades\Artisan::call('config:clear');
    \Illuminate\Support\Facades\Artisan::call('view:clear');
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    return \Illuminate\Support\Facades\Artisan::output();
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


// SSL Verification routes
Route::get('.well-known/acme-challenge/waD09KWIIjC_hcgt2yAdWSzpxNjqUxByArxtEhOEamc', function () {
    return response("waD09KWIIjC_hcgt2yAdWSzpxNjqUxByArxtEhOEamc.T6FstPqiJ8YOJoti0U2zWVggn5eDpoHSWhnDmgXywY8")
        ->header("content", "text/plain");
});
Route::get('well-known/acme-challenge/Dsd3bi2Z_iW_sC7Ls4mIOY8xOLZ6_e38XILGqclUZJw', function () {
    return "Dsd3bi2Z_iW_sC7Ls4mIOY8xOLZ6_e38XILGqclUZJw.T6FstPqiJ8YOJoti0U2zWVggn5eDpoHSWhnDmgXywY8";
});
